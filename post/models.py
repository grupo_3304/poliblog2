from django.db import models
from django.conf import settings
from django.utils import timezone


class Post(models.Model):
    name = models.CharField(max_length=255)
    release_date = models.DateTimeField(default=timezone.now)
    poster_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name} ({self.release_date})'


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    release_date = models.DateTimeField(default=timezone.now)
    likes = models.IntegerField(default=0)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255, default="")
    descricao_categoria = models.CharField(max_length=255, default="")
    post = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}'