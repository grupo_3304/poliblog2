from django.forms import ModelForm
from .models import Post, Comment, Category


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            #'release_date',
            'poster_url',
        ]
        labels = {
            'name': 'Título',
            #'release_date': 'Data de publicação',
            'poster_url': 'URL do Poster',
        }
    
class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
           # 'release_date',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            #'release_date': 'Data de publicação',
            'text': 'Comentário',
        }

class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = [
            'name', 
            'descricao_categoria',
            'post',
        ]

        labels = {
            'name': 'Nome da categoria',
            'descricao_categoria': 'Descrição da categoria',
            'post': 'Notícias que serão incluídas na categoria',
        }