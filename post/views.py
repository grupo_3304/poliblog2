from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .temp_data import post_data
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm
from django.urls import reverse_lazy, reverse
from django.views import generic

class PostListView(generic.ListView):
    model = Post
    template_name = 'post/index.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'post/detail.html'
    def get_object(self, **kwargs):
        pk = self.kwargs.get('pk')
        return get_object_or_404(Post, pk=pk)

def detail_post(request, post_id):
    post = Post.objects.get(pk=post_id)
    context = {'post': post}
    return render(request, 'post/detail.html', context)

def detail_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    context = {'post': post}
    return render(request, 'post/detail.html', context)

def search_post(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        post_list = Post.objects.filter(name__icontains=search_term)
        context = {"post_list": post_list}
    return render(request, 'post/search.html', context)


def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post_name = form.cleaned_data['name']
            post_release_date = form.cleaned_data['release_date']
            post_poster_url = form.cleaned_data['poster_url']
            post = Post(name=post_name,
                          release_date=post_release_date,
                          poster_url=post_poster_url)
            post.save()
            return HttpResponseRedirect(
                reverse('post:detail', args=(post.id, )))
    else:
        form = PostForm()
    context = {'form': form}
    return render(request, 'post/create.html', context)

def update_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post.name = form.cleaned_data['name']
            #post.release_date = form.cleaned_data['release_date']
            post.poster_url = form.cleaned_data['poster_url']
            post.save()
            return HttpResponseRedirect(
                reverse('post:detail', args=(post.id, )))
    else:
        form = PostForm(
            initial={
                'name': post.name,
                #'release_date': post.release_date,
                'poster_url': post.poster_url
            })

    context = {'post': post, 'form': form}
    return render(request, 'post/update.html', context)


def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        post.delete()
        return HttpResponseRedirect(reverse('post:index'))

    context = {'post': post}
    return render(request, 'post/delete.html', context)

#class PostDeleteView(DeleteView):
#    model = Post
#    template_name = 'post/delete.html'
#    succes_url = reverse_lazy('index')

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            #comment_release_date = form.cleaned_data['release_date']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('post:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'post/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'post/categorias.html'


class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'post/create_categoria.html'
    fields = ['name','author', 'descricao_categoria', 'post']
    success_url = reverse_lazy('post:categorias')