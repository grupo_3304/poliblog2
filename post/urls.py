from django.urls import path

from . import views

app_name = 'post'

urlpatterns = [
    path('', views.PostListView.as_view(), name='index'),
    #path('', views.PostDetailView.as_view(), name='detail'),
    path('search/', views.search_post, name='search'),
    path('create/', views.create_post, name='create'),
    path('<int:post_id>/', views.detail_post, name='detail'),
    path('update/<int:post_id>/', views.update_post, name='update'),
    path('delete/<int:post_id>/', views.delete_post, name='delete'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('categorias/', views.CategoryListView.as_view(), name='categorias'),
    path('categorias/create_categoria', views.CategoryCreateView.as_view(), name='create_categoria'),
]