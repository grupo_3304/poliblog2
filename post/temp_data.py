post_data = [{
    "id":
    "1",
    "name":
    "Pesquisadores da Poli-USP estão entre os mais influentes da ciência mundial",
    "release_date":
    "04NOV2022",
    "poster_url":
    "https://www.poli.usp.br/wp-content/uploads/2022/11/50427963166_8ea00db51d_h-1536x1020.jpg"
}, {
    "id":
    "2",
    "name":
    "Estudantes da Engenharia de Petróleo visitam fábrica de equipamentos submarinos",
    "release_date":
    "03NOV2022",
    "poster_url":
    "https://www.poli.usp.br/wp-content/uploads/2022/11/Visita_a_Baker-1536x1020.jpeg"
}, {
    "id":
    "3",
    "name":
    "Professores da Poli-USP e da Unesp publicam livro sobre Transferência de Calor para engenharia",
    "release_date":
    "28OUT2022",
    "poster_url":
    "https://www.poli.usp.br/wp-content/uploads/2022/10/exatas-simoes-fundamentos-transferencia-whatsapp_1-768x768.png"
}]
